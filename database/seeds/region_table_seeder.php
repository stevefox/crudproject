<?php

use Illuminate\Database\Seeder;

class region_table_seeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('region')->insert([
            
            'name' => 'Region I',
            'companyreg' => 'Addessa Corporation'

            // 'region' => Str::random(10).'@gmail.com',
            // 'name' => Str::random(10),
            // 'password' => Hash::make('password'),
            
        ]);

        DB::table('region')->insert([
            
            'name' => 'Region II',
            'companyreg' => 'MIA Corporation'

        ]);

        DB::table('region')->insert([
            
            'name' => 'Region III',
            'companyreg' => 'PAN Corporation'

        ]);
    }
}
