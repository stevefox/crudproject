<?php

namespace App\Http\Controllers;

use App\Employee; //<-------Include Model Table Name-------
use Illuminate\Http\Request;


class EmployeeController extends Controller
{
    public function index (){     

        $employees = Employee::all();
        return view('employees.index', compact('employees'));
        //Controller - foldername/filename
        
    }

    public function create (){

        return view('employees.create');

    }

    public function store(request $req){


        $employee = new Employee;

        $employee->regid = '1';
        $employee->firstname = $req->firstname;
        $employee->lastname = $req->lastname;
        $employee->save();

        return redirect()->route('employees.index');

    }

    public function edit ($id){
      
        $employees = Employee::find($id);
        return view('employees.edit', compact('employees'));

        
    }


    public function update (request $req, $id){
    
        $employees = Employee::find($id);
        $employees->firstname = $req->firstname;
        $employees->lastname = $req->lastname;
        $employees->update();

        return redirect()->route('employees.index');

        
    }

    public function delete($id){

        $employee = Employee::find($id);
        $employee->delete();

        return redirect()->route('employees.index');

    }
}
