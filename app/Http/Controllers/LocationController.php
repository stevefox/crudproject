<?php

namespace App\Http\Controllers;
use App\Location;
use Illuminate\Http\Request;

class LocationController extends Controller
{
   public function index(){

    $locations =Location::all();
    return view('locations.index', compact('locations'));   
    

   }
   public function create(){

     return view('locations.create');

   }
   public function store(request $req){
    $location = new Location;
    $location->name = $req->name;
    $location->address = $req->address;
    $location->status = $req->status;
    $location->save();
    return redirect()->route('locations.index');

    

   }
    public function edit($id){
    
    $location = Location::find($id);
    return view('locations.edit', compact('location'));
     

    }
    public function update(request $req, $id){
        $location = Location::find($id);
        $location->name = $req->name;
        $location->address = $req->address;
        $location->status = $req->status;
        $location->update();
        return redirect()->route('locations.index');
        
     

    }
    public function delete(request $req,$id){
    
        $location = Location::find($id);
        $location->delete();
        return redirect()->route('locations.index');
         
    
        }
}
