<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms - Create</h2>

<form action=" {{ route('companies.store') }}" method='POST'>
{{ csrf_field() }}

 <label for="lname">Location:</label><br>
    <select name="location" id="">
      @foreach ($locations as $location)
        <option value=" {{ $location->id }} "> {{ $location->name}} </option>
      @endforeach
    </select><br>

  <label for="name">Name:</label><br>
  <input type="text"  name="name"><br>
  
  <label for="lname">Status:</label><br>
  <select name="status" id="status">
    <option value="1">Available</option>
    <option value="2">Not Available</option>
  </select>  

  <br><br>

  <input type="submit" value="Submit">

</form> 

 

</body>
</html>
