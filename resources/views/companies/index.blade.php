<h1>Sample Crud - Index</h1>

<a href=" {{ route('companies.create') }}">ADD</a>

<table border="2">
  <tr>
    <th>Id</th>
    <th>Location Id</th>
    <th>Name</th>
    <th>Status</th>
    <th>Function</th>
  </tr>

  @foreach($companies as $company)
    <tr>
      <td><span id="companyid">{{ $company->id  }}</span> </td>
      <td> {{ $company->location ? $company->location->name : 'None' }} </td>
      <td> {{ $company->name  }} </td>
      <td> 

      @if($company->status == 1)
        <span style="color:green;">Available</span>
      @else
        <span style="color:red;">Not Available</span>
      @endif
     
      </td>
      <td>
      <a href="{{ route('companies.edit', ['id' => $company->id]) }}">Edit</a>
      <form id="frm{{ $company->id }}" method="post" action="{{ route('companies.delete', ['id' => $company->id]) }}">
        {{ csrf_field() }}
      <a href="#" onclick="myFunction('{{ $company->id  }}')">Delete</a>

      <script>
        function myFunction(id) {
          // console.log(id);
          var r = confirm("Do you want to delete?");
          if (r == true) {
            document.getElementById('frm'+id).submit();
            // console.log(document.getElementById("frm"+id));
            // console.log("frm"+id);
          } 
        }
        </script>
       </form>
      </td> 
    </tr>
   @endforeach


</table>