<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms - Create</h2>

<form action=" {{ route('locations.store') }}" method='POST'>
{{ csrf_field() }}

  <label for="name">Name:</label><br>
  <input type="text"  name="name"><br>
  <label for="name">Address:</label><br>
  <textarea name="address"></textarea><br>
  
  <label for="lname">Status:</label><br>
  <select name="status" id="status">
    <option value="1">Available</option>
    <option value="2">Not Available</option>
  </select>  

  <br><br>

  <input type="submit" value="Submit">

</form> 

 

</body>
</html>
