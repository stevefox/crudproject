<!DOCTYPE html>
<html>
<body>

<h2>HTML Forms - Edit</h2>

<form action=" {{ route('employees.update', ['id' => $employees->id]) }} " method='POST'>
{{ csrf_field() }}

  <label for="fname">Firstname:</label><br>
  <input type="text"  name="firstname" value="{{ $employees->firstname}}"><br>
  <label for="lname">Lastname:</label><br>
  <input type="text"  name="lastname" value="{{ $employees->lastname}}"><br><br>

  <input type="submit" value="Submit">

</form> 

 

</body>
</html>
